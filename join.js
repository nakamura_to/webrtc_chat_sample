(function (exports) {
  'use strict';

  exports.Join = Join;

  // default key
  var KEY = '';

  // default user name
  var NAME = 'unknown';

  // default signaling server host
  var HOST = location.host;

  // default ice servers config
  var ICE_SERVERS_CONFIG = [ {'url': 'stun: stun.l.google.com:19302' } ];

  // default media config
  var MEDIA_CONFIG = { video: true, audio: true };

  // default channels config
  var CHANNELS_CONFIG = [];

  // default logger
  var LOGGER = function (level, message) {
    console.log('[join][' + level + ']: ' + message);
  };

  // adapters
  var RTCPeerConnection = window.RTCPeerConnection || window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
  var RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription;
  var RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate;
  var getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
  if (getUserMedia) {
    getUserMedia = getUserMedia.bind(navigator);
  } else {
    throw new Error('navigator.getUserMedia is not supported');
  }

  // utility
  var util = {
    inherits: function (ctor, superCtor) {
      ctor.prototype = Object.create(superCtor.prototype, {
        constructor: {
          value: ctor,
          enumerable: false,
          writable: true,
          configurable: true
        }
      });
    }
  };

  /**
   * WebRTC simplified API
   */
  function Join(options) {
    EventEmitter.call(this);

    options = options || {};

    this.key = options.key || KEY;
    this.local = new Local(null, options.name || NAME);
    this._host = options.host || HOST;
    this._secure = options.secure || false;
    this._iceServersConfig = options._iceServers || ICE_SERVERS_CONFIG;
    this._mediaConfig = options.media || MEDIA_CONFIG;
    this._channelsConfig = options.channels || CHANNELS_CONFIG;
    this._logger = options.logger || LOGGER;

    this._connections = {};
    this._bundles = {};

    var signalingChannel = new SignalingChannel(this._host, this._secure);
    this._setupSignalingChannel(signalingChannel);
    signalingChannel.start();
  }

  util.inherits(Join, EventEmitter);

  Join.prototype._setupSignalingChannel = function (signalingChannel) {
    var join = this;
    var iceServersConfig = this._iceServersConfig;
    var logger = this._logger;
    var key = this.key;
    var local = this.local;
    var localStream;

    signalingChannel.on('open', function () {
      logger('debug', 'signalingChannel opened');
      signalingChannel.send({type: 'connect', key: key, src: local});
    });

    signalingChannel.on('connect', function (signal) {
      local.id = signal.id;
      logger('debug', 'signalingChannel connected: [' + local.id + ']');
      join.emit('local', local);
      join._getUserMedia(function (stream) {
        localStream = stream;
        signal.peers.forEach(function (peer) {
          var connection = createConnection(join._channelsConfig, peer);
          connection.createOffer();
        });        
      });
    });

    signalingChannel.on('icecandidate', function (signal) {
      logger('debug', 'accept icecandidate: [' + local.id + '<-' + signal.src.id + ']');
      var connection = findConnection(signal.src);
      connection.addIceCandidate(signal.candidate);      
    });

    signalingChannel.on('offer', function (signal) {
      logger('debug', 'accept offer: [' + local.id + '<-' + signal.src.id + ']');
      var connection = createConnection(signal.channelsConfig, signal.src);
      connection.setRemoteDescription(signal.offer);
      connection.createAnswer();      
    });

    signalingChannel.on('answer', function (signal) {
      logger('debug', 'accept answer: [' + local.id + '<-' + signal.src.id + ']');
      var connection = findConnection(signal.src);
      connection.setRemoteDescription(signal.answer);      
    });

    signalingChannel.on('unknown', function (signal) {
      logger('debug', 'unknown signal type: ' + signal.type);
    });

    function createConnection(channelsConfig, peer) {
      var remote = new Remote(peer.id, peer.name);
      var connection = new Connection(createRTCPeerConnection(), remote);    
      join._setupConnection(signalingChannel, localStream, connection);
      join.emit('remote', connection.remote);
      channelsConfig.forEach(function (config, i) {
        createChannel(connection, config, i);
      });
      return connection;
    }

    function createRTCPeerConnection() {
      try {
        return new RTCPeerConnection({iceServers: iceServersConfig});
      } catch (error) {
        logger('warn', 'failed to new RTCPeerConnection: ' + error);
        // TODO: optional should be configurable for browsers which don't support SCTP
        return new RTCPeerConnection({iceServers: iceServersConfig}, { optional:[ { RtpDataChannels: true } ]});       
      }
    }

    function createChannel(connection, config, id) {
      var label = Object.keys(config)[0];
      var option = Object.create(config[label]);
      option.id = id;
      option.negotiated = true;
      var channel = connection.createChannel(label, option);
      join._setupChannel(channel);      
    }

    function findConnection(peer) {
      if (join._connections[peer.id]) {
        return join._connections[peer.id];
      }
      throw new Error('connection [' + peer.id +'] not found');
    }

  };

  Join.prototype._getUserMedia = function (next) {
    var join = this;
    var local = this.local;
    var logger = this._logger;
    var mediaConfig = this._mediaConfig;

    if (mediaConfig.video || mediaConfig.audio) {
      getUserMedia(
        mediaConfig,
        function (stream) {
          local.emit('stream', stream);
          next(stream);
        },
        function (warn) {
          // TODO: this error should be handled by user
          logger('warn', warn.name + ': ' + warn.message);
          next(null);
        });
    } else {
      next(null);
    }
  };

  Join.prototype._setupConnection = function (signalingChannel, stream, connection) {
    var join = this;
    var local = this.local;
    var remote = connection.remote;
    var channelsConfig = this._channelsConfig;
    var logger = this._logger;

    logger('debug', 'peer connection opend: [' + local.id + '<->' + remote.id + ']');

    add(connection);

    connection.on('icecandidate', function (event) {
      if (event.candidate) {
        logger('debug', 'icecandidate: [' + local.id + '->' + remote.id + ']');
        signalingChannel.send({
          type: 'icecandidate', 
          candidate: event.candidate, 
          src: local, 
          dest: remote
        });      
      }
    });

    connection.on('offer', function (offer) {
      logger('debug', 'offer: [' + local.id + '->' + remote.id + ']');
      signalingChannel.send({
        type: 'offer', 
        offer: offer,
        channelsConfig: channelsConfig,
        src: local, 
        dest: remote
      });
    });

    connection.on('answer', function (answer) {
      logger('debug', 'answer: [' + local.id + '->' + remote.id + ']');
      signalingChannel.send({
        type: 'answer', 
        answer: answer, 
        src: local, 
        dest: remote
      });
    });

    connection.on('iceconnectionstatechange', function (event) {
      var state = event.target.iceConnectionState;
      logger('debug', 'iceconnectionstatechange: ' + state + ': [' + local.id + '->' + remote.id + ']');
      if (state === 'disconnected' || state === 'failed') {
        remote.close();
      }      
    });

    connection.on('addstream', function (event) {
      logger('debug', 'addstream');
      remote.emit('stream', event.stream);
    });

    connection.on('close', function () {
      logger('debug', 'peer connection closed: [' + local.id + '-x-' + remote.id + ']');
      remove(connection);
    });

    connection.on('error', function (error) {
      logger('error', error);
      remote.close();
    });

    if (stream) {
      connection.addStream(stream);
    }

    function add(connection) {
      join._connections[connection.remote.id] = connection;
    }

    function remove(connection) {
      delete join._connections[connection.remote.id];
    }
  };

  Join.prototype._setupChannel = function (channel) {
    var join = this;
    var local = this.local; 
    var remote = channel.remote;
    var label = channel.label;
    var bundle = this._bundles[label];
    var logger = this._logger;

    if (!bundle) {
      bundle = new Bundle(label, local);
      join._bundles[label] = bundle;
      bundle.on('dispose', function () {
        delete join._bundles[label];
      });
      join.emit('channel.' + label, bundle);
    }

    bundle.add(channel);

    channel.on('open', function (event) {
      logger('debug', 'channel opened: [' + label + ': ' + local.id + '<->' + remote.id + ']');
      bundle.emit('open', {remote: remote});
    });

    channel.on('message', function (event) {
      logger('debug', 'channel message received: [' + label + ': ' + local.id + '<-' + remote.id + ']');
      bundle.emit('data', {remote: remote, data: event.data});
    });

    channel.on('close', function (event) {
      logger('debug', 'channel closed: [' + label + ': ' + local.id + '-x-' + remote.id + ']');
      bundle.remove(channel);
      bundle.emit('close', {remote: remote});
    });

    channel.on('error', function (error) {
      logger('debug', 'channel error: [' + label + ': ' + local.id + '-?-' + remote.id + ']');
      bundle.remove(channel);
      bundle.emit('error', {remote: remote, error: error});
    });

    channel.on('sending', function () {
      logger('debug', 'channel sending: [' + label + ': ' + local.id + '->' + remote.id + ']');
    });
  };

  /**
   * websocket signaling channel
   */
  function SignalingChannel(host, secure) {
    EventEmitter.call(this);

    this._url = (secure ? 'wss' : 'ws') + '://' + host;
    this._webSocket = null;
  }

  util.inherits(SignalingChannel, EventEmitter);

  SignalingChannel.prototype.start = function () {
    var signalingChannel = this;
    var url = this._url;
    var webSocket = new WebSocket(url);
    var signalTypes = ['connect', 'icecandidate', 'offer', 'answer'];

    webSocket.onopen = function (event) {
      signalingChannel.emit('open', event);
    };

    webSocket.onmessage = function (event) {
      var signal = JSON.parse(event.data);
      var type = signalTypes.indexOf(signal.type) === -1 ? 'unknown' : signal.type;
      signalingChannel.emit(type, signal);
    };

    this._webSocket = webSocket;
  };

  SignalingChannel.prototype.send = function (obj) {
    this._webSocket.send(JSON.stringify(obj));
  };

  /**
   * abstract peer
   */
  function Peer(id, name) {
    EventEmitter.call(this);

    this.id = id;
    this.name = name;
  }

  util.inherits(Peer, EventEmitter);

  /**
   * local peer
   */
  function Local(id, name) {
    Peer.call(this, id, name);
  }

  util.inherits(Local, Peer);

  /**
   * remote peer
   */
  function Remote(id, name) {
    Peer.call(this, id, name);
  }

  util.inherits(Remote, Peer);

  Remote.prototype.close = function() {
    this.emit('close');
  };

  /**
   * wrapper for RTCPeerConnection
   */
  function Connection(pc, remote) {
    EventEmitter.call(this);

    var connection = this;
    remote.on('close', function () {
      connection.close();
    });

    this.remote = remote;
    this._pc = pc;

    this._initRTCPeerConnection();
  }

  util.inherits(Connection, EventEmitter);

  Connection.prototype._initRTCPeerConnection = function () {
    var connection = this;
    var pc = this._pc;

    pc.onicecandidate = function (event) {
      connection.emit('icecandidate', event);
    };

    pc.oniceconnectionstatechange = function (event) {
      connection.emit('iceconnectionstatechange', event);
    };

    pc.onnegotiationneeded = function(event) {
      connection.emit('negotiationneeded', event);
    };

    pc.onaddstream = function (event) {
      connection.emit('addstream', event);
    };
  };

  Connection.prototype.createOffer = function () {
    var connection = this;
    var pc = this._pc;

    pc.createOffer(
      function (offer) {
        pc.setLocalDescription(offer);
        connection.emit('offer', offer);
      },
      function (error) {
        connection.emit('error', error);
      }
    );
  };

  Connection.prototype.createAnswer = function () {
    var connection = this;
    var pc = this._pc;

    pc.createAnswer(
      function (answer) {
        pc.setLocalDescription(answer);
        connection.emit('answer', answer);
      },
      function (error) {
        connection.emit('error', error);
      }
    );    
  };

  Connection.prototype.addIceCandidate = function (candidate) {
    this._pc.addIceCandidate(new RTCIceCandidate(candidate));
  };

  Connection.prototype.setRemoteDescription = function (sdp) {
    var connection = this;
    var pc = this._pc;

    pc.setRemoteDescription(
      new RTCSessionDescription(sdp),
      function () {
        // do nothing
      },
      function (error) {
        connection.emit('error', error);
      }
    );
  };

  Connection.prototype.createChannel = function (label, options) {
    var connection = this;
    var dc = this._pc.createDataChannel(label, options);
    return new Channel(dc, connection.remote);
  };

  Connection.prototype.addStream = function (stream) {
    this._pc.addStream(stream);
  };

  Connection.prototype.close = function () {
    this._pc.close();
    this.emit('close');
  };

  /**
   * wrapper for RTCDataChannel
   */
  function Channel(dc, remote) {
    EventEmitter.call(this);

    this._dc = dc;
    this.label = dc.label;
    this.remote = remote;

    this._initRTCDataChannel();
  }

  util.inherits(Channel, EventEmitter);

  Channel.prototype._initRTCDataChannel = function () {
    var channel = this;
    var dc = this._dc;

    dc.onopen = function (event) {
      channel.emit('open', event);
    };

    dc.onmessage = function (event) {
      channel.emit('message', event);
    };

    dc.onclose = function(event) {
      channel.emit('close', event);
    };

    dc.onerror = function(error) {
      channel.emit('error', error);
    };
  };

  Channel.prototype.send = function (message) {
    var channel = this;
    var dc = this._dc;

    channel.emit('sending');
    dc.send(message);
  };

  Channel.prototype.close = function () {
    this._dc.close();
    this.emit('close');
  };

  /**
   * bundle of same label channels.
   */
  function Bundle(label, local) {
    EventEmitter.call(this);

    this.label = label;
    this.local = local;
    this._channels = [];
  }

  util.inherits(Bundle, EventEmitter);

  Bundle.prototype.add = function (channel) {
    this._channels.push(channel);
  };

  Bundle.prototype.remove = function (channel) {
    var pos = this._channels.indexOf(channel);
    if (pos !== -1) {
      this._channels.splice(pos, 1);
    }
  };

  Bundle.prototype.size = function () {
    return this._channels.length;
  };

  Bundle.prototype.send = function (message) {
    this._channels.forEach(function (channel) {
      channel.send(message);
    });
  };

  Bundle.prototype.dispose = function () {
    this._channels.forEach(function (channel) {
      channel.close();
    });
    this._channels = null;
    this.emit('dispose');
  };

  /**
   * simple event emitter 
   */
  function EventEmitter() {
    this._events = {};
  }

  EventEmitter.prototype.on = function (type, listener) {
    if (this._events[type]) {
      this._events[type].push(listener);
    } else {
      this._events[type] = [listener];
    }
  };

  EventEmitter.prototype.emit = function (type) {
    var args = Array.prototype.slice.call(arguments, 1);
    var listeners = this._events[type] || [];

    listeners.forEach(function (listener) {
      listener.apply(this, args);
    });
  };

}(window));
